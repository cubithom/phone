if ( !application ) {
	application = {};
}
if ( !application.mixins ) {
	application.mixins = {};
}

application.mixins.countryDialCodes = {
	data: function () {
		return {
			countries: [
			  {
			    name: '%{countries:Afghanistan}',
			    iso2: 'af',
			    dialCode: '93'
			  },
			  {
			    name: '%{countries:Albania}',
			    iso2: 'al',
			    dialCode: '355'
			  },
			  {
			    name: '%{countries:Algeria}',
			    iso2: 'dz',
			    dialCode: '213'
			  },
			  {
			    name: '%{countries:American Samoa}',
			    iso2: 'as',
			    dialCode: '1684'
			  },
			  {
			    name: '%{countries:Andorra}',
			    iso2: 'ad',
			    dialCode: '376'
			  },
			  {
			    name: '%{countries:Angola}',
			    iso2: 'ao',
			    dialCode: '244'
			  },
			  {
			    name: '%{countries:Anguilla}',
			    iso2: 'ai',
			    dialCode: '1264'
			  },
			  {
			    name: '%{countries:Antigua and Barbuda}',
			    iso2: 'ag',
			    dialCode: '1268'
			  },
			  {
			    name: '%{countries:Argentina}',
			    iso2: 'ar',
			    dialCode: '54'
			  },
			  {
			    name: '%{countries:Armenia}',
			    iso2: 'am',
			    dialCode: '374'
			  },
			  {
			    name: '%{countries:Aruba}',
			    iso2: 'aw',
			    dialCode: '297'
			  },
			  {
			    name: '%{countries:Australia}',
			    iso2: 'au',
			    dialCode: '61',
			    priority: 0
			  },
			  {
			    name: '%{countries:Austria}',
			    iso2: 'at',
			    dialCode: '43'
			  },
			  {
			    name: '%{countries:Azerbaijan}',
			    iso2: 'az',
			    dialCode: '994'
			  },
			  {
			    name: '%{countries:Bahamas}',
			    iso2: 'bs',
			    dialCode: '1242'
			  },
			  {
			    name: '%{countries:Bahrain}',
			    iso2: 'bh',
			    dialCode: '973'
			  },
			  {
			    name: '%{countries:Bangladesh}',
			    iso2: 'bd',
			    dialCode: '880'
			  },
			  {
			    name: '%{countries:Barbados}',
			    iso2: 'bb',
			    dialCode: '1246'
			  },
			  {
			    name: '%{countries:Belarus}',
			    iso2: 'by',
			    dialCode: '375'
			  },
			  {
			    name: '%{countries:Belgium}',
			    iso2: 'be',
			    dialCode: '32'
			  },
			  {
			    name: '%{countries:Belize}',
			    iso2: 'bz',
			    dialCode: '501'
			  },
			  {
			    name: '%{countries:Benin}',
			    iso2: 'bj',
			    dialCode: '229'
			  },
			  {
			    name: '%{countries:Bermuda}',
			    iso2: 'bm',
			    dialCode: '1441'
			  },
			  {
			    name: '%{countries:Bhutan}',
			    iso2: 'bt',
			    dialCode: '975'
			  },
			  {
			    name: '%{countries:Bolivia}',
			    iso2: 'bo',
			    dialCode: '591'
			  },
			  {
			    name: '%{countries:Bosnia and Herzegovina}',
			    iso2: 'ba',
			    dialCode: '387'
			  },
			  {
			    name: '%{countries:Botswana}',
			    iso2: 'bw',
			    dialCode: '267'
			  },
			  {
			    name: '%{countries:Brazil}',
			    iso2: 'br',
			    dialCode: '55'
			  },
			  {
			    name: '%{countries:British Indian Ocean Territory}',
			    iso2: 'io',
			    dialCode: '246'
			  },
			  {
			    name: '%{countries:British Virgin Islands}',
			    iso2: 'vg',
			    dialCode: '1284'
			  },
			  {
			    name: '%{countries:Brunei}',
			    iso2: 'bn',
			    dialCode: '673'
			  },
			  {
			    name: '%{countries:Bulgaria}',
			    iso2: 'bg',
			    dialCode: '359'
			  },
			  {
			    name: '%{countries:Burkina Faso}',
			    iso2: 'bf',
			    dialCode: '226'
			  },
			  {
			    name: '%{countries:Burundi}',
			    iso2: 'bi',
			    dialCode: '257'
			  },
			  {
			    name: '%{countries:Cambodia}',
			    iso2: 'kh',
			    dialCode: '855'
			  },
			  {
			    name: '%{countries:Cameroon}',
			    iso2: 'cm',
			    dialCode: '237'
			  },
			  {
			    name: '%{countries:Canada}',
			    iso2: 'ca',
			    dialCode: '1',
			    priority: 1,
			    areaCodes: ['204', '226', '236', '249', '250', '289', '306', '343', '365', '387', '403', '416', '418', '431', '437', '438', '450', '506', '514', '519', '548', '579', '581', '587', '604', '613', '639', '647', '672', '705', '709', '742', '778', '780', '782', '807', '819', '825', '867', '873', '902', '905']
			  },
			  {
			    name: '%{countries:Cape Verde}',
			    iso2: 'cv',
			    dialCode: '238'
			  },
			  {
			    name: '%{countries:Caribbean Netherlands}',
			    iso2: 'bq',
			    dialCode: '599',
			    priority: 1
			  },
			  {
			    name: '%{countries:Cayman Islands}',
			    iso2: 'ky',
			    dialCode: '1345'
			  },
			  {
			    name: '%{countries:Central African Republic}',
			    iso2: 'cf',
			    dialCode: '236'
			  },
			  {
			    name: '%{countries:Chad}',
			    iso2: 'td',
			    dialCode: '235'
			  },
			  {
			    name: '%{countries:Chile}',
			    iso2: 'cl',
			    dialCode: '56'
			  },
			  {
			    name: '%{countries:China}',
			    iso2: 'cn',
			    dialCode: '86'
			  },
			  {
			    name: '%{countries:Christmas Island}',
			    iso2: 'cx',
			    dialCode: '61',
			    priority: 2
			  },
			  {
			    name: '%{countries:Cocos}',
			    iso2: 'cc',
			    dialCode: '61',
			    priority: 1
			  },
			  {
			    name: '%{countries:Colombia}',
			    iso2: 'co',
			    dialCode: '57'
			  },
			  {
			    name: '%{countries:Comoros}',
			    iso2: 'km',
			    dialCode: '269'
			  },
			  {
			    name: '%{countries:Congo}',
			    iso2: 'cd',
			    dialCode: '243'
			  },
			  {
			    name: '%{countries:Congo}',
			    iso2: 'cg',
			    dialCode: '242'
			  },
			  {
			    name: '%{countries:Cook Islands}',
			    iso2: 'ck',
			    dialCode: '682'
			  },
			  {
			    name: '%{countries:Costa Rica}',
			    iso2: 'cr',
			    dialCode: '506'
			  },
			  {
			    name: '%{countries:Côte d’Ivoire}',
			    iso2: 'ci',
			    dialCode: '225'
			  },
			  {
			    name: '%{countries:Croatia}',
			    iso2: 'hr',
			    dialCode: '385'
			  },
			  {
			    name: '%{countries:Cuba}',
			    iso2: 'cu',
			    dialCode: '53'
			  },
			  {
			    name: '%{countries:Curaçao}',
			    iso2: 'cw',
			    dialCode: '599',
			    priority: 0
			  },
			  {
			    name: '%{countries:Cyprus}',
			    iso2: 'cy',
			    dialCode: '357'
			  },
			  {
			    name: '%{countries:Czech Republic}',
			    iso2: 'cz',
			    dialCode: '420'
			  },
			  {
			    name: '%{countries:Denmark}',
			    iso2: 'dk',
			    dialCode: '45'
			  },
			  {
			    name: '%{countries:Djibouti}',
			    iso2: 'dj',
			    dialCode: '253'
			  },
			  {
			    name: '%{countries:Dominica}',
			    iso2: 'dm',
			    dialCode: '1767'
			  },
			  {
			    name: '%{countries:Dominican Republic}',
			    iso2: 'do',
			    dialCode: '1',
			    priority: 2,
			    areaCodes: ['809', '829', '849']
			  },
			  {
			    name: '%{countries:Ecuador}',
			    iso2: 'ec',
			    dialCode: '593'
			  },
			  {
			    name: '%{countries:Egypt}',
			    iso2: 'eg',
			    dialCode: '20'
			  },
			  {
			    name: '%{countries:El Salvador}',
			    iso2: 'sv',
			    dialCode: '503'
			  },
			  {
			    name: '%{countries:Equatorial Guinea}',
			    iso2: 'gq',
			    dialCode: '240'
			  },
			  {
			    name: '%{countries:Eritrea}',
			    iso2: 'er',
			    dialCode: '291'
			  },
			  {
			    name: '%{countries:Estonia}',
			    iso2: 'ee',
			    dialCode: '372'
			  },
			  {
			    name: '%{countries:Ethiopia}',
			    iso2: 'et',
			    dialCode: '251'
			  },
			  {
			    name: '%{countries:Falkland Islands}',
			    iso2: 'fk',
			    dialCode: '500'
			  },
			  {
			    name: '%{countries:Faroe Islands}',
			    iso2: 'fo',
			    dialCode: '298'
			  },
			  {
			    name: '%{countries:Fiji}',
			    iso2: 'fj',
			    dialCode: '679'
			  },
			  {
			    name: '%{countries:Finland}',
			    iso2: 'fi',
			    dialCode: '358',
			    priority: 0
			  },
			  {
			    name: '%{countries:France}',
			    iso2: 'fr',
			    dialCode: '33'
			  },
			  {
			    name: '%{countries:French Guiana}',
			    iso2: 'gf',
			    dialCode: '594'
			  },
			  {
			    name: '%{countries:French Polynesia}',
			    iso2: 'pf',
			    dialCode: '689'
			  },
			  {
			    name: '%{countries:Gabon}',
			    iso2: 'ga',
			    dialCode: '241'
			  },
			  {
			    name: '%{countries:Gambia}',
			    iso2: 'gm',
			    dialCode: '220'
			  },
			  {
			    name: '%{countries:Georgia}',
			    iso2: 'ge',
			    dialCode: '995'
			  },
			  {
			    name: '%{countries:Germany}',
			    iso2: 'de',
			    dialCode: '49'
			  },
			  {
			    name: '%{countries:Ghana}',
			    iso2: 'gh',
			    dialCode: '233'
			  },
			  {
			    name: '%{countries:Gibraltar}',
			    iso2: 'gi',
			    dialCode: '350'
			  },
			  {
			    name: '%{countries:Greece}',
			    iso2: 'gr',
			    dialCode: '30'
			  },
			  {
			    name: '%{countries:Greenland}',
			    iso2: 'gl',
			    dialCode: '299'
			  },
			  {
			    name: '%{countries:Grenada}',
			    iso2: 'gd',
			    dialCode: '1473'
			  },
			  {
			    name: '%{countries:Guadeloupe}',
			    iso2: 'gp',
			    dialCode: '590',
			    priority: 0
			  },
			  {
			    name: '%{countries:Guam}',
			    iso2: 'gu',
			    dialCode: '1671'
			  },
			  {
			    name: '%{countries:Guatemala}',
			    iso2: 'gt',
			    dialCode: '502'
			  },
			  {
			    name: '%{countries:Guernsey}',
			    iso2: 'gg',
			    dialCode: '44',
			    priority: 1
			  },
			  {
			    name: '%{countries:Guinea}',
			    iso2: 'gn',
			    dialCode: '224'
			  },
			  {
			    name: '%{countries:Guinea-Bissau}',
			    iso2: 'gw',
			    dialCode: '245'
			  },
			  {
			    name: '%{countries:Guyana}',
			    iso2: 'gy',
			    dialCode: '592'
			  },
			  {
			    name: '%{countries:Haiti}',
			    iso2: 'ht',
			    dialCode: '509'
			  },
			  {
			    name: '%{countries:Honduras}',
			    iso2: 'hn',
			    dialCode: '504'
			  },
			  {
			    name: '%{countries:Hong Kong}',
			    iso2: 'hk',
			    dialCode: '852'
			  },
			  {
			    name: '%{countries:Hungary}',
			    iso2: 'hu',
			    dialCode: '36'
			  },
			  {
			    name: '%{countries:Iceland}',
			    iso2: 'is',
			    dialCode: '354'
			  },
			  {
			    name: '%{countries:India}',
			    iso2: 'in',
			    dialCode: '91'
			  },
			  {
			    name: '%{countries:Indonesia}',
			    iso2: 'id',
			    dialCode: '62'
			  },
			  {
			    name: '%{countries:Iran}',
			    iso2: 'ir',
			    dialCode: '98'
			  },
			  {
			    name: '%{countries:Iraq}',
			    iso2: 'iq',
			    dialCode: '964'
			  },
			  {
			    name: '%{countries:Ireland}',
			    iso2: 'ie',
			    dialCode: '353'
			  },
			  {
			    name: '%{countries:Isle of Man}',
			    iso2: 'im',
			    dialCode: '44',
			    priority: 2
			  },
			  {
			    name: '%{countries:Israel}',
			    iso2: 'il',
			    dialCode: '972'
			  },
			  {
			    name: '%{countries:Italy}',
			    iso2: 'it',
			    dialCode: '39',
			    priority: 0
			  },
			  {
			    name: '%{countries:Jamaica}',
			    iso2: 'jm',
			    dialCode: '1876'
			  },
			  {
			    name: '%{countries:Japan}',
			    iso2: 'jp',
			    dialCode: '81'
			  },
			  {
			    name: '%{countries:Jersey}',
			    iso2: 'je',
			    dialCode: '44',
			    priority: 3
			  },
			  {
			    name: '%{countries:Jordan}',
			    iso2: 'jo',
			    dialCode: '962'
			  },
			  {
			    name: '%{countries:Kazakhstan}',
			    iso2: 'kz',
			    dialCode: '7',
			    priority: 1
			  },
			  {
			    name: '%{countries:Kenya}',
			    iso2: 'ke',
			    dialCode: '254'
			  },
			  {
			    name: '%{countries:Kiribati}',
			    iso2: 'ki',
			    dialCode: '686'
			  },
			  {
			    name: '%{countries:Kosovo}',
			    iso2: 'xk',
			    dialCode: '383'
			  },
			  {
			    name: '%{countries:Kuwait}',
			    iso2: 'kw',
			    dialCode: '965'
			  },
			  {
			    name: '%{countries:Kyrgyzstan}',
			    iso2: 'kg',
			    dialCode: '996'
			  },
			  {
			    name: '%{countries:Laos}',
			    iso2: 'la',
			    dialCode: '856'
			  },
			  {
			    name: '%{countries:Latvia}',
			    iso2: 'lv',
			    dialCode: '371'
			  },
			  {
			    name: '%{countries:Lebanon}',
			    iso2: 'lb',
			    dialCode: '961'
			  },
			  {
			    name: '%{countries:Lesotho}',
			    iso2: 'ls',
			    dialCode: '266'
			  },
			  {
			    name: '%{countries:Liberia}',
			    iso2: 'lr',
			    dialCode: '231'
			  },
			  {
			    name: '%{countries:Libya}',
			    iso2: 'ly',
			    dialCode: '218'
			  },
			  {
			    name: '%{countries:Liechtenstein}',
			    iso2: 'li',
			    dialCode: '423'
			  },
			  {
			    name: '%{countries:Lithuania}',
			    iso2: 'lt',
			    dialCode: '370'
			  },
			  {
			    name: '%{countries:Luxembourg}',
			    iso2: 'lu',
			    dialCode: '352'
			  },
			  {
			    name: '%{countries:Macau}',
			    iso2: 'mo',
			    dialCode: '853'
			  },
			  {
			    name: '%{countries:Macedonia}',
			    iso2: 'mk',
			    dialCode: '389'
			  },
			  {
			    name: '%{countries:Madagascar}',
			    iso2: 'mg',
			    dialCode: '261'
			  },
			  {
			    name: '%{countries:Malawi}',
			    iso2: 'mw',
			    dialCode: '265'
			  },
			  {
			    name: '%{countries:Malaysia}',
			    iso2: 'my',
			    dialCode: '60'
			  },
			  {
			    name: '%{countries:Maldives}',
			    iso2: 'mv',
			    dialCode: '960'
			  },
			  {
			    name: '%{countries:Mali}',
			    iso2: 'ml',
			    dialCode: '223'
			  },
			  {
			    name: '%{countries:Malta}',
			    iso2: 'mt',
			    dialCode: '356'
			  },
			  {
			    name: '%{countries:Marshall Islands}',
			    iso2: 'mh',
			    dialCode: '692'
			  },
			  {
			    name: '%{countries:Martinique}',
			    iso2: 'mq',
			    dialCode: '596'
			  },
			  {
			    name: '%{countries:Mauritania}',
			    iso2: 'mr',
			    dialCode: '222'
			  },
			  {
			    name: '%{countries:Mauritius}',
			    iso2: 'mu',
			    dialCode: '230'
			  },
			  {
			    name: '%{countries:Mayotte}',
			    iso2: 'yt',
			    dialCode: '262',
			    priority: 1
			  },
			  {
			    name: '%{countries:Mexico}',
			    iso2: 'mx',
			    dialCode: '52'
			  },
			  {
			    name: '%{countries:Micronesia}',
			    iso2: 'fm',
			    dialCode: '691'
			  },
			  {
			    name: '%{countries:Moldova}',
			    iso2: 'md',
			    dialCode: '373'
			  },
			  {
			    name: '%{countries:Monaco}',
			    iso2: 'mc',
			    dialCode: '377'
			  },
			  {
			    name: '%{countries:Mongolia}',
			    iso2: 'mn',
			    dialCode: '976'
			  },
			  {
			    name: '%{countries:Montenegro}',
			    iso2: 'me',
			    dialCode: '382'
			  },
			  {
			    name: '%{countries:Montserrat}',
			    iso2: 'ms',
			    dialCode: '1664'
			  },
			  {
			    name: '%{countries:Morocco}',
			    iso2: 'ma',
			    dialCode: '212',
			    priority: 0
			  },
			  {
			    name: '%{countries:Mozambique}',
			    iso2: 'mz',
			    dialCode: '258'
			  },
			  {
			    name: '%{countries:Myanmar}',
			    iso2: 'mm',
			    dialCode: '95'
			  },
			  {
			    name: '%{countries:Namibia}',
			    iso2: 'na',
			    dialCode: '264'
			  },
			  {
			    name: '%{countries:Nauru}',
			    iso2: 'nr',
			    dialCode: '674'
			  },
			  {
			    name: '%{countries:Nepal}',
			    iso2: 'np',
			    dialCode: '977'
			  },
			  {
			    name: '%{countries:Netherlands}',
			    iso2: 'nl',
			    dialCode: '31'
			  },
			  {
			    name: '%{countries:New Caledonia}',
			    iso2: 'nc',
			    dialCode: '687'
			  },
			  {
			    name: '%{countries:New Zealand}',
			    iso2: 'nz',
			    dialCode: '64'
			  },
			  {
			    name: '%{countries:Nicaragua}',
			    iso2: 'ni',
			    dialCode: '505'
			  },
			  {
			    name: '%{countries:Niger}',
			    iso2: 'ne',
			    dialCode: '227'
			  },
			  {
			    name: '%{countries:Nigeria}',
			    iso2: 'ng',
			    dialCode: '234'
			  },
			  {
			    name: '%{countries:Niue}',
			    iso2: 'nu',
			    dialCode: '683'
			  },
			  {
			    name: '%{countries:Norfolk Island}',
			    iso2: 'nf',
			    dialCode: '672'
			  },
			  {
			    name: '%{countries:North Korea}',
			    iso2: 'kp',
			    dialCode: '850'
			  },
			  {
			    name: '%{countries:Northern Mariana Islands}',
			    iso2: 'mp',
			    dialCode: '1670'
			  },
			  {
			    name: '%{countries:Norway}',
			    iso2: 'no',
			    dialCode: '47',
			    priority: 0
			  },
			  {
			    name: '%{countries:Oman}',
			    iso2: 'om',
			    dialCode: '968'
			  },
			  {
			    name: '%{countries:Pakistan}',
			    iso2: 'pk',
			    dialCode: '92'
			  },
			  {
			    name: '%{countries:Palau}',
			    iso2: 'pw',
			    dialCode: '680'
			  },
			  {
			    name: '%{countries:Palestine}',
			    iso2: 'ps',
			    dialCode: '970'
			  },
			  {
			    name: '%{countries:Panama}',
			    iso2: 'pa',
			    dialCode: '507'
			  },
			  {
			    name: '%{countries:Papua New Guinea}',
			    iso2: 'pg',
			    dialCode: '675'
			  },
			  {
			    name: '%{countries:Paraguay}',
			    iso2: 'py',
			    dialCode: '595'
			  },
			  {
			    name: '%{countries:Peru}',
			    iso2: 'pe',
			    dialCode: '51'
			  },
			  {
			    name: '%{countries:Philippines}',
			    iso2: 'ph',
			    dialCode: '63'
			  },
			  {
			    name: '%{countries:Poland}',
			    iso2: 'pl',
			    dialCode: '48'
			  },
			  {
			    name: '%{countries:Portugal}',
			    iso2: 'pt',
			    dialCode: '351'
			  },
			  {
			    name: '%{countries:Puerto Rico}',
			    iso2: 'pr',
			    dialCode: '1',
			    priority: 3,
			    areaCodes: ['787', '939']
			  },
			  {
			    name: '%{countries:Qatar}',
			    iso2: 'qa',
			    dialCode: '974'
			  },
			  {
			    name: '%{countries:Réunion}',
			    iso2: 're',
			    dialCode: '262',
			    priority: 0
			  },
			  {
			    name: '%{countries:Romania}',
			    iso2: 'ro',
			    dialCode: '40'
			  },
			  {
			    name: '%{countries:Russia}',
			    iso2: 'ru',
			    dialCode: '7',
			    priority: 0
			  },
			  {
			    name: '%{countries:Rwanda}',
			    iso2: 'rw',
			    dialCode: '250'
			  },
			  {
			    name: '%{countries:Saint Barthélemy}',
			    iso2: 'bl',
			    dialCode: '590',
			    priority: 1
			  },
			  {
			    name: '%{countries:Saint Helena}',
			    iso2: 'sh',
			    dialCode: '290'
			  },
			  {
			    name: '%{countries:Saint Kitts and Nevis}',
			    iso2: 'kn',
			    dialCode: '1869'
			  },
			  {
			    name: '%{countries:Saint Lucia}',
			    iso2: 'lc',
			    dialCode: '1758'
			  },
			  {
			    name: '%{countries:Saint Martin}',
			    iso2: 'mf',
			    dialCode: '590',
			    priority: 2
			  },
			  {
			    name: '%{countries:Saint Pierre and Miquelon}',
			    iso2: 'pm',
			    dialCode: '508'
			  },
			  {
			    name: '%{countries:Saint Vincent and the Grenadines}',
			    iso2: 'vc',
			    dialCode: '1784'
			  },
			  {
			    name: '%{countries:Samoa}',
			    iso2: 'ws',
			    dialCode: '685'
			  },
			  {
			    name: '%{countries:San Marino}',
			    iso2: 'sm',
			    dialCode: '378'
			  },
			  {
			    name: '%{countries:São Tomé and Príncipe}',
			    iso2: 'st',
			    dialCode: '239'
			  },
			  {
			    name: '%{countries:Saudi Arabia}',
			    iso2: 'sa',
			    dialCode: '966'
			  },
			  {
			    name: '%{countries:Senegal}',
			    iso2: 'sn',
			    dialCode: '221'
			  },
			  {
			    name: '%{countries:Serbia}',
			    iso2: 'rs',
			    dialCode: '381'
			  },
			  {
			    name: '%{countries:Seychelles}',
			    iso2: 'sc',
			    dialCode: '248'
			  },
			  {
			    name: '%{countries:Sierra Leone}',
			    iso2: 'sl',
			    dialCode: '232'
			  },
			  {
			    name: '%{countries:Singapore}',
			    iso2: 'sg',
			    dialCode: '65'
			  },
			  {
			    name: '%{countries:Sint Maarten}',
			    iso2: 'sx',
			    dialCode: '1721'
			  },
			  {
			    name: '%{countries:Slovakia}',
			    iso2: 'sk',
			    dialCode: '421'
			  },
			  {
			    name: '%{countries:Slovenia}',
			    iso2: 'si',
			    dialCode: '386'
			  },
			  {
			    name: '%{countries:Solomon Islands}',
			    iso2: 'sb',
			    dialCode: '677'
			  },
			  {
			    name: '%{countries:Somalia}',
			    iso2: 'so',
			    dialCode: '252'
			  },
			  {
			    name: '%{countries:South Africa}',
			    iso2: 'za',
			    dialCode: '27'
			  },
			  {
			    name: '%{countries:South Korea}',
			    iso2: 'kr',
			    dialCode: '82'
			  },
			  {
			    name: '%{countries:South Sudan}',
			    iso2: 'ss',
			    dialCode: '211'
			  },
			  {
			    name: '%{countries:Spain}',
			    iso2: 'es',
			    dialCode: '34'
			  },
			  {
			    name: '%{countries:Sri Lanka}',
			    iso2: 'lk',
			    dialCode: '94'
			  },
			  {
			    name: '%{countries:Sudan}',
			    iso2: 'sd',
			    dialCode: '249'
			  },
			  {
			    name: '%{countries:Suriname}',
			    iso2: 'sr',
			    dialCode: '597'
			  },
			  {
			    name: '%{countries:Svalbard and Jan Mayen}',
			    iso2: 'sj',
			    dialCode: '47',
			    priority: 1
			  },
			  {
			    name: '%{countries:Swaziland}',
			    iso2: 'sz',
			    dialCode: '268'
			  },
			  {
			    name: '%{countries:Sweden}',
			    iso2: 'se',
			    dialCode: '46'
			  },
			  {
			    name: '%{countries:Switzerland}',
			    iso2: 'ch',
			    dialCode: '41'
			  },
			  {
			    name: '%{countries:Syria}',
			    iso2: 'sy',
			    dialCode: '963'
			  },
			  {
			    name: '%{countries:Taiwan}',
			    iso2: 'tw',
			    dialCode: '886'
			  },
			  {
			    name: '%{countries:Tajikistan}',
			    iso2: 'tj',
			    dialCode: '992'
			  },
			  {
			    name: '%{countries:Tanzania}',
			    iso2: 'tz',
			    dialCode: '255'
			  },
			  {
			    name: '%{countries:Thailand}',
			    iso2: 'th',
			    dialCode: '66'
			  },
			  {
			    name: '%{countries:Timor-Leste}',
			    iso2: 'tl',
			    dialCode: '670'
			  },
			  {
			    name: '%{countries:Togo}',
			    iso2: 'tg',
			    dialCode: '228'
			  },
			  {
			    name: '%{countries:Tokelau}',
			    iso2: 'tk',
			    dialCode: '690'
			  },
			  {
			    name: '%{countries:Tonga}',
			    iso2: 'to',
			    dialCode: '676'
			  },
			  {
			    name: '%{countries:Trinidad and Tobago}',
			    iso2: 'tt',
			    dialCode: '1868'
			  },
			  {
			    name: '%{countries:Tunisia}',
			    iso2: 'tn',
			    dialCode: '216'
			  },
			  {
			    name: '%{countries:Turkey}',
			    iso2: 'tr',
			    dialCode: '90'
			  },
			  {
			    name: '%{countries:Turkmenistan}',
			    iso2: 'tm',
			    dialCode: '993'
			  },
			  {
			    name: '%{countries:Turks and Caicos Islands}',
			    iso2: 'tc',
			    dialCode: '1649'
			  },
			  {
			    name: '%{countries:Tuvalu}',
			    iso2: 'tv',
			    dialCode: '688'
			  },
			  {
			    name: '%{countries:U.S. Virgin Islands}',
			    iso2: 'vi',
			    dialCode: '1340'
			  },
			  {
			    name: '%{countries:Uganda}',
			    iso2: 'ug',
			    dialCode: '256'
			  },
			  {
			    name: '%{countries:Ukraine}',
			    iso2: 'ua',
			    dialCode: '380'
			  },
			  {
			    name: '%{countries:United Arab Emirates}',
			    iso2: 'ae',
			    dialCode: '971'
			  },
			  {
			    name: '%{countries:United Kingdom}',
			    iso2: 'gb',
			    dialCode: '44',
			    priority: 0
			  },
			  {
			    name: '%{countries:United States}',
			    iso2: 'us',
			    dialCode: '1',
			    priority: 0
			  },
			  {
			    name: '%{countries:Uruguay}',
			    iso2: 'uy',
			    dialCode: '598'
			  },
			  {
			    name: '%{countries:Uzbekistan}',
			    iso2: 'uz',
			    dialCode: '998'
			  },
			  {
			    name: '%{countries:Vanuatu}',
			    iso2: 'vu',
			    dialCode: '678'
			  },
			  {
			    name: '%{countries:Vatican City}',
			    iso2: 'va',
			    dialCode: '39',
			    priority: 1
			  },
			  {
			    name: '%{countries:Venezuela}',
			    iso2: 've',
			    dialCode: '58'
			  },
			  {
			    name: '%{countries:Vietnam}',
			    iso2: 'vn',
			    dialCode: '84'
			  },
			  {
			    name: '%{countries:Wallis and Futuna}',
			    iso2: 'wf',
			    dialCode: '681'
			  },
			  {
			    name: '%{countries:Western Sahara}',
			    iso2: 'eh',
			    dialCode: '212',
			    priority: 1
			  },
			  {
			    name: '%{countries:Yemen}',
			    iso2: 'ye',
			    dialCode: '967'
			  },
			  {
			    name: '%{countries:Zambia}',
			    iso2: 'zm',
			    dialCode: '260'
			  },
			  {
			    name: '%{countries:Zimbabwe}',
			    iso2: 'zw',
			    dialCode: '263'
			  },
			  {
			    name: '%{countries:Åland Islands}',
			    iso2: 'ax',
			    dialCode: '358',
			    priority: 1
			  }
			]
		}
	}
}