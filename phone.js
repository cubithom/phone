application.components.Phone = Vue.component("n-form-phone", {
	template: "#n-form-phone",
	mixins: [application.mixins.countryDialCodes],
	props: {
		value: {
			required: true
		},
		label: {
			type: String,
			required: false
		},
		// whether or not you are in edit mode
		edit: {
			type: Boolean,
			required: false,
			default: true
		},
		required: {
			type: Boolean,
			required: false,
			// explicitly set default value to null, otherwise vue will make it false which we can't distinguish from "not set"
			default: null
		},
		name: {
			type: String,
			required: false
		},
		placeholderCountryCode: {
			type: String,
			required: false
		},
		placeholderPhone: {
			type: String,
			required: false
		},
		// a json schema component stating the definition
		schema: {
			type: Object,
			required: false
		},
		pattern: {
			type: String,
			required: false
		},
		patternComment: {
			type: String,
			required: false
		},	
		disabled: {
			type: Boolean,
			required: false
		},
		validator: {
			type: Function,
			required: false
		},
		timeout: {
			type: Number,
			required: false
		},
		mode: {
			type: String,
			required: false
		},
		info: {
			type: String,
			required: false
		},
		infoIcon: {
			type: String,
			required: false
		},
		suffix: {
			type: String,
			required: false
		},
		before: {
			type: String,
			required: false
		},
		after: {
			type: String,
			required: false
		},
		allow: {
			type: Function,
			required: false
		},
		// comma separated list of iso2 countries?
		countryRestrictions: {
			type: String,
			required: false
		},
		defaultCountryCode: {
			type: String,
			required: false
		}
	},
	data: function() {
		return {
			messages: [],
			valid: null,			
			phone: null,
			countryCode: null,
			valid: null
		};
	},
	created: function () {
		this.initialize();
	},
	computed: {
		definition: function() {
			return nabu.utils.vue.form.definition(this);
		},
		mandatory: function() {
			return nabu.utils.vue.form.mandatory(this);
		}
	},	
	methods: {
		initialize: function () {
			if ( !this.value ) {
				this.phone = null;
			}
			else if (this.value && this.value.indexOf("+") == 0 && this.value.length >= 4) {
				var selectedCountryCode = this.filterCountryCode(this.countryCode.dialCode)[0];
				if ( selectedCountryCode && selectedCountryCode.length == 1 ) {
					this.countryCode = selectedCountryCode;
					this.phone = this.value.substring(1 + this.countryCode.length); // take the part of value that comes after the dialCode
				}
				// When we have multiple hits, only fill in the phone field
				else if ( selectedCountryCode && selectedCountryCode.length > 1 ){
					this.phone = this.value;
				}
			}
		},
		formatCountryCode: function (country) {
			return "+" + country.dialCode;
		},
		filterCountryCode: function (search) {
			return this.countries.filter(function (country) {
				var searchDialCode = search ? search.replace("+", "") : search;
				var dialCodeMatch = country.dialCode.toLowerCase().indexOf(searchDialCode) >= 0;
				var nameMatch = country.name.toLowerCase().indexOf(search) >= 0;
				var iso2Match = country.iso2.toLowerCase().indexOf(search) >= 0;
				return nameMatch || iso2Match || dialCodeMatch;
			});
		},
		updateCountryCode: function (value) {
			this.countryCode = value;
			this.update();
		},
		updatePhone: function (value) {
			this.phone = value;
			this.update();
		},
		update: function () {
			var formattedCountryCode = this.countryCode ? ("+" + this.countryCode.dialCode.toString()) : "";
			var formattedPhone = this.phone ? this.phone : "";
			
			var formatted = formattedCountryCode + formattedPhone;
			if (formatted != this.value) {
				this.$emit("input", formatted);
			}
		},
		validate: function(soft) {
			// reset current messages
			this.messages.splice(0);
			this.valid = null;
			
			// this performs all basic validation and enriches the messages array to support asynchronous
			var messages = nabu.utils.schema.json.validate(this.definition, this.value, this.mandatory);
			// add context to the messages to we can link back to this component
			for (var i = 0; i < messages.length; i++) {
				// components are vue-based entities that have recursive links to each other, the validation messages again etc
				// we don't want to include them in the enumerable properties cause this would prevent them from ever being serialized
				// we want to keep all state serializable and validations can become part of the state
				Object.defineProperty(messages[i], 'component', {
					value: this,
					enumerable: false
				});
			}
			messages = nabu.utils.schema.addAsyncValidation([]);
			// allow for custom validation
			messages = nabu.utils.vue.form.validateCustom(messages, this.value, this.validator, this);

			var self = this;
			
			messages.then(function(validations) {
				
				// nabu.utils.vue.form.rewriteCodes(messages, self.codes);
				var hardMessages = messages.filter(function(x) { return !x.soft });
				// if we are doing a soft validation and all messages were soft, set valid to unknown
				if (soft && hardMessages.length == 0 && (messages.length > 0 || !this.value) && self.valid == null) {
					self.valid = null;
				}
				else {
					self.valid = messages.length == 0;
					nabu.utils.arrays.merge(self.messages, nabu.utils.vue.form.localMessages(self, messages));
				}
				// make sure we emit the value we just validated. in case of validate on blur (or a short validate timeout) and a longer emit timeout
				// we might be validating values that are not persisted, we can then browse to the next page without persisting it at all
				// don't send out a belated update
				if (self.timer) {
					clearTimeout(self.timer);
					self.timer = null;
				}
			});
			
			this.valid = false;
			
			return messages;
		}
	}
});