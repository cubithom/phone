<template id="n-form-phone">
	<div class="n-form-phone n-form-component">
		<div class="n-form-phone-form">
			<slot name="top"></slot>
			<div class="n-form-label-wrapper" v-if="label || info">
				<slot name="label" :label="label" :mandatory="mandatory">
					<label class="n-form-label" :class="{ 'n-form-input-required': mandatory }" v-if="label" v-html="label"></label>
				</slot>
				<n-info class="n-form-label-info" :icon="infoIcon" v-if="info"><span v-html="info"></span></n-info>
			</div>
			<slot name="before" :content="before">
				<div class="n-form-component-before" v-if="before" v-html="before"></div>
			</slot>		
			
			<div class="n-form-input-wrapper">
				<n-input-combo 
					:value="countryCode"
					@input="updateCountryCode"
					:timeout="timeout"
					:filter="filterCountryCode"
					:formatter="formatCountryCode"
					class="countryCode"
					:class="{ 'n-form-valid': valid != null && valid, 'n-form-invalid': valid != null && !valid }"
					placeholder="%{countryCode}"
					>
					
					<template slot="value" scope="props" class="country-code-entry">
						<div class="iti-flag" :class="props.value.iso2"></div>
						<div class="country-name">{{props.value.name}}</div>
						<div class="country-code">(+{{props.value.dialCode}})</div>
					</template>
					
				</n-input-combo>
					
				<n-form-text
					:value="phone"
					@input="updatePhone"
					:timeout="timeout"
					class="phone"
					:placeholder="placeholderPhone"
					:class="{ 'n-form-valid': valid != null && valid, 'n-form-invalid': valid != null && !valid }">
					</n-form-text><slot name="suffix"><div class="n-form-suffix" v-if="suffix" v-html="suffix"></div></slot><span class="n-input-result"></span>
			</div>

			<slot name="messages" :messages="messages">
				<n-messages :messages="messages" v-if="messages && messages.length"/>
			</slot>
			<slot name="after" :content="after">
				<div class="n-form-component-after" v-if="after" v-html="after"></div>
			</slot>
			<slot name="bottom"></slot>
		</div>
	</div>
</template>